DOTFILES=(.emacs.d .tmux.conf .zshrc .zshrc.custom)

for file in ${DOTFILES[@]}
do
    if [ ! -e $HOME/$file ]; then
        ln -s $HOME/dotfiles/$file $HOME/$file
    fi
done

# install oh-my-zsh
[ ! -d ~/.oh-my-zsh ] && git clone git://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
