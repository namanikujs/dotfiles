;; ~/.emacs.d/ に load-path を通す
(add-to-list 'load-path "~/.emacs.d/")
;; (add-to-list 'load-path "~/.emacs.d/plugins")

;; el-init
(add-to-list 'load-path "~/.emacs.d/el-init")
(require 'el-init)
(setq el-init:load-directory-list '("base" "pkg" ("ext" t)))
(el-init:load "~/.emacs.d/inits")
(put 'upcase-region 'disabled nil)
