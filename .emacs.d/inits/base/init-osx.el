;;; for OSX
;; Command-Key and Option-Key
(when (eq system-type 'darwin)
  (tool-bar-mode 0)
  (setq default-directory "~/" ) 

  ;; Fix default-directory
  (setq inhibit-splash-screen t)
  (defun cd-to-homedir-all-buffers ()
    "Change every current directory of all buffers to the home directory."
    (mapc
     (lambda (buf) (set-buffer buf) (cd (expand-file-name "~"))) (buffer-list)))
  (add-hook 'after-init-hook 'cd-to-homedir-all-buffers)

  ;; 英語y
  (set-face-attribute 'default nil
                      :family "Menlo" ;; font
                      :height 150)    ;; font size
  ;; 日本語
  (set-fontset-font
   nil 'japanese-jisx0208
   ;; (font-spec :family "Hiragino Mincho Pro")) ;; font
   (font-spec :family "Hiragino Kaku Gothic ProN")) ;; font

  ;; 半角と全角の比を1:2にしたければ
  (setq face-font-rescale-alist
        ;;        '((".*Hiragino_Mincho_pro.*" . 1.2)))
        '((".*Hiragino_Kaku_Gothic_ProN.*" . 1.2)));; Mac用フォント設定

  
  (setq mac-option-modifier 'meta)
  ;; drag&dropで新しいwindowが開かないようにする
  (define-key global-map [ns-drag-file] [ns-open-file])
  (setq ns-pop-up-frames nil)
  ;; PATH
  (setenv "PATH" (concat "/Library/Frameworks/Python.framework/Versions/2.7/bin/:/opt/local/bin:/usr/local/bin:" (getenv "PATH")))
  (setq exec-path
        (append '("/Library/Frameworks/Python.framework/Versions/2.7/bin/"
                  "/opt/local/bin"
                  "/usr/local/bin")
                exec-path)))

(provide 'init-osx)
