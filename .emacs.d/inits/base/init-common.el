;; ;; $HOMEを開くようにする
;; (cd "~/")

;; C-hをbackspaceにする
(global-set-key (kbd "C-h") 'delete-backward-char)
(global-set-key (kbd "C-'") 'help-command)

;; syntax highlightingのためのおまじない
(global-font-lock-mode t)
(setq font-lock-maximum-decoration t)

;; regionに色をつける
(setq transent-mark-mode t)

(if (boundp 'window-system)
    (setq initial-frame-alist
          (append (list
                   '(cursor-color     . "gray")
                   '(cursor-type      . box)
                   '(menu-bar-lines . 0)
                   '(vertical-scroll-bars . nil) ;;スクロールバーはいらない
                   )
                  initial-frame-alist)))
(setq default-frame-alist initial-frame-alist)

;; カラーテーマを読み込む
(load-theme 'manoj-dark)

;; カーソウルのある行をハイライト
(defface hlline-face
  '((((class color)
      (background dark))
     (:background "dark slate gray"))
    (((class color)
      (background light))
     ;; (:background "moccasin")
     (:background "SlateGray"))
    (t
     ()))
  "*Face used by hl-line.")

(setq hl-line-face 'hlline-face)
;; (setq hl-line-face 'underline) ; 下線
(global-hl-line-mode)
(put 'set-goal-column 'disabled nil)
(put 'downcase-region 'disabled nil)

;; cua-mode
;; 矩形操作を向上
(cua-mode t)
(setq cua-enable-cua-keys nil)

;; タブ設定
(setq-default tab-width 4 indent-tabs-mode nil)

;; auto-save
(add-to-list 'backup-directory-alist
             (cons "." "~/.emacs.d/backups/"))
(setq auto-save-file-name-transforms
      `((".*" ,(expand-file-name "~/.emacs.d/backups/") t)))
;; (setq auto-save-timeout 15)
;; (setq auto-save-interval 60)

(add-hook 'after-save-hook
          'executable-make-buffer-file-executable-if-script-p)

;; windmove
(windmove-default-keybindings) ; 引数なしの場合は Shift

;; ;; winner-mode
;; (when (fboundp 'winner-mode)
;;   (winner-mode t))

;; 再帰エラー対策
(setq max-specpdl-size 6000)
(setq max-lisp-eval-depth 1000)

;; mouse
(unless window-system
  (require 'mouse)
  (xterm-mouse-mode t)
  (global-set-key [mouse-4] '(lambda ()
                              (interactive)
                              (scroll-down 1)))
  (global-set-key [mouse-5] '(lambda ()
                              (interactive)
                              (scroll-up 1)))
  (defun track-mouse (e))
  (setq mouse-sel-mode t))

(provide 'init-common)
