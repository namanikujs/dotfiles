;; anything.Eli
(require 'anything-startup)

;; 'anything-filelist+
(global-set-key (kbd "C-;") 'anything-filelist+)
(setq anything-c-filelist-file-name "/tmp/all.filelist")

(provide 'init-anything)
