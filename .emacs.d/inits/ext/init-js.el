;; js2-mode
;; (autoload 'js2-mode "js2" nil t)
(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))

;; jshint
;; (setq exec-path (append exec-path '("/usr/local/bin")))
(add-hook 'js2-mode-hook '(lambda ()
          (require 'flymake-jshint)
          (flymake-jshint-load)))

;; Coffee script
(autoload 'coffee-mode "coffee-mode" "Major mode for editing CoffeeScript." t)
(add-to-list 'auto-mode-alist '("\\.coffee$" . coffee-mode))
(add-to-list 'auto-mode-alist '("Cakefile" . coffee-mode))

(setq coffee-tab-width 2)

(provide 'init-js)
