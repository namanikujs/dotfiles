;; auto-complete.el
(require 'auto-complete-config)
(global-auto-complete-mode t)

;; 自動起動しないようにする
(setq ac-auto-start nil)

;; TABで補完起動
(ac-set-trigger-key "TAB")

;; "M-/"で補完中止
(define-key ac-completing-map "\M-/" 'ac-stop)

;; 補完メニュー表示時のみC-n/C-pで補完候補を選択する
(setq ac-use-menu-map t)
(define-key ac-menu-map "\C-n" 'ac-next)
(define-key ac-menu-map "\C-p" 'ac-previous)

;; ;; ac-anything.el
;; (require 'ac-anything)
;; (define-key ac-complete-mode-map (kbd "C-:") 'ac-complete-with-anything)

(provide 'init-auto-complete)
