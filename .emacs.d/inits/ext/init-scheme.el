(when (eq system-type 'darwin)
  (setq geiser-racket-binary "/Applications/Racket v6.0.1/bin/racket"))
(setq geiser-active-implementations '(racket))

(provide 'init-scheme)
