;; open-junk-file
(require 'open-junk-file)
;; ファイル名およびファイル場所の指定
(setq open-junk-file-format "~/junk/%Y-%m-%d/%H%M%S.") 
(global-set-key (kbd "C-x C-z") 'open-junk-file)

;; uniquify.el
(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)
(setq uniquify-ignore-buffers-re "*[^*]+*")

;; popwin.el
(require 'popwin)
(setq display-buffer-function 'popwin:display-buffer)

;; wdired.el
(require 'wdired)
(define-key dired-mode-map "r" 'wdired-change-to-wdired-mode)
(setq wdired-allow-to-change-permissions t)

;; quickrun.el
(require 'quickrun)
;; (push '("*quickrun*") popwin:special-display-config)
;; (push '("*eshell*") popwin:special-display-config)
(global-set-key (kbd "C-c C-v") 'quickrun)
(quickrun-add-command "go"
                      '((:command . "/usr/local/go/bin/go")
                        (:exec    . "%c run %o %s %a"))
                        :mode 'go-mode)

;; expand-region.el
(require 'expand-region)
(global-set-key (kbd "C-@") 'er/expand-region)
(global-set-key (kbd "C-M-@") 'er/contract-region) ;; リージョンを狭める
;; transient-mark-modeが nilでは動作しませんので注意
(transient-mark-mode t)

(provide 'init-misc)
