;; C/C++
(add-hook 'c-mode-hook
          (function (lambda ()
                      (c-set-style "k&r")
                      (setq c-basic-offset 4)
                      (setq indent-tabs-mode nil))))

(add-hook 'c++-mode-hook
          '(lambda ()
             (c-set-style "stroustrup")
             (setq indent-tabs-mode nil)
             (c-set-offset 'innamespace 0)
             (c-set-offset 'arglist-close 0)))

(setq auto-mode-alist
      (append '(("\\.h$" . c++-mode)
                ("\\.hpp$" . c++-mode)
                ("\\.cc$" . c++-mode)
                ("\\.cpp$" . c++-mode))
              auto-mode-alist))

(provide 'init-cc)
