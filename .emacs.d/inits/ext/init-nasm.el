(autoload 'nasm-mode "~/.emacs.d/vendor/nasm-mode.el" "" t)
(add-to-list 'auto-mode-alist '("\\.\\(nasm\\|s\\)$" . nasm-mode))
(setq nasm-basic-offset 4)

(provide 'init-nasm)
