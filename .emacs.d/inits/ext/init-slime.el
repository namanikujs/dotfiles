;; Setup load-path, autoloads and your lisp system
(add-to-list 'load-path "~/.emacs.d/vendor/slime")
(require 'slime-autoloads)
;; (setq inferior-lisp-program "/opt/sbcl/bin/sbcl")
(setq inferior-lisp-program "/usr/local/bin/ccl")


(provide 'init-slime)
