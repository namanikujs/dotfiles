(require 'auto-highlight-symbol)
(global-auto-highlight-symbol-mode t)

(provide 'init-highlight)
