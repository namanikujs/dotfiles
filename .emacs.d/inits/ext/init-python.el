;; Python
;; (require 'ipython)
;; (require 'virtualenv)

(add-hook 'python-mode-hook
          (function (lambda ()
                      (setq py-indent-offset 4)
                      (setq indent-tabs-mode nil)
                      (setq show-trailing-whitespace t)
                      ;; quickrun
                      ;; (local-set-key (kbd "C-c C-v") 'quickrun)
                      )))

;; ipython
;; (setq
;;  python-shell-interpreter "ipython"
;;  python-shell-interpreter-args ""
;;  python-shell-prompt-regexp "In \\[[0-9]+\\]: "
;;  python-shell-prompt-output-regexp "Out\\[[0-9]+\\]: "
;;  python-shell-completion-setup-code
;;    "from IPython.core.completerlib import module_completion"
;;  python-shell-completion-module-string-code
;;    "';'.join(module_completion('''%s'''))\n"
;;  python-shell-completion-string-code
;;    "';'.join(get_ipython().Completer.all_completions('''%s'''))\n")

;; (setq python-shell-completion-string-code
;;       "';'.join(__IP.complete('''%s'''))\n"
;;       python-shell-completion-module-string-code "")


;; flymake
(require 'flymake-python-pyflakes)
(add-hook 'python-mode-hook 'flymake-python-pyflakes-load)
(setq flymake-python-pyflakes-executable 
      (if (eq system-type 'darwin)
          "/Library/Frameworks/Python.framework/Versions/2.7/bin/flake8"
        "flake8"))

;; virtualenv
(require 'virtualenv)

;; info-look
(require 'info-look)
(info-lookup-add-help
 :mode 'python-mode
 :regexp "[[:alnum:]_]+"
 :doc-spec
 '(("(python)Index" nil "")))


;; Complete the closing pair
(defun electric-pair ()
  "Insert character pair without sournding spaces"
  (interactive)
  (let (parens-require-spaces)
    (insert-pair)))

;; python.el
(add-hook
 'python-mode-hook
 '(lambda ()
    (define-key python-mode-map "\C-m" 'newline-and-indent)
    (define-key python-mode-map "\M-\"" 'electric-pair)
    (define-key python-mode-map "\M-\'" 'electric-pair)
    (define-key python-mode-map "\M-[" 'electric-pair)
    (define-key python-mode-map "\M-{" 'electric-pair)
    (define-key python-mode-map "\M-(" 'electric-pair)
    (define-key inferior-python-mode-map "\t" 'python-complete-symbol)))
